# FrontendKit
Базовый проект для быстрой сборки LandingPage и более сложных проектов.
Базово уже подключен и инициализирован скрипт для обработки источников и utm-меток - Sourcebuster.js

Для его настройки необходимо перейти в head страницы и указать правильный хост, на котором будет размещен сайт (по умолчанию localhost).

Реализована всплывающая форма захвата на основе Fancybox 3 и уже настроена валидация базовых полей (имя, email, телефон).
В данную сборку включены и настроены следующие компоненты:

**CSS**

- Normalize.css - для сброса всех стилей

**JavaScript**

*  Sourcebuster - http://sbjs.rocks/sourcebuster/configure
*  Fancybox 3 - http://fancyapps.com/fancybox/3/
*  Single page nav - https://github.com/ChrisWojcik/single-page-nav
*  Jquery Validation Plugin - https://jqueryvalidation.org

---

**Update 22.04.2017**

- Подключен gulp-typograf (https://github.com/typograf/gulp-typograf)
- Подключен gulp-newer
- Подключен gulp-plumber для лога ошибок
- Оптимизированы некоторые задачи


**Update 20.03.2017**

- Настроена базовая валидация в форме захвата
- Подключен gulp-svgo
- Подключен gulp-clean-css
