$(function(){

    // Smart filter range price
    $(".range_input").ionRangeSlider({
        type: "double",
        grid: true,
        min: 0,
        max: 100000
    });



    try {
        $('.detail-slider-big').slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1
        });
    } catch (e) {
        console.log(e);
    }







    $('.add-to-cart').click(function () {
        UIkit.notification("Товар успешно добавлен в корзину", {pos: 'top-center'})
    });



    /*
     * Обработчик кол-ва товаров
     */

    $('.product-count__minus').click(function () {
        var tmp = $(this).parent().find('.product-count__input');
        var count = parseInt(tmp.val());
        if(count > 0) {
            tmp.val(count - 1);
        }
    });
    $('.product-count__plus').click(function () {
        var tmp = $(this).parent().find('.product-count__input');
        tmp.val(parseInt(tmp.val()) + 1);
    });





    /**
     * Показывем модальное окно, если
     * была нажата кнопка с классом .showModal
     * и подставляем в него данные из атрибутов data-
     */
    $(".showModal").click(function()
    {
        var modal = $("#modal_form");

        var title = $(this).attr('data-title');
        if(title !== undefined) {
            modal.find('.modal_title').text(title);
        }


        var subtitle = $(this).attr('data-subtitle');
        if(subtitle !== undefined) {
            modal.find('.modal_subtitle').text(subtitle);
        }

        var btn = $(this).attr('data-button');
        if(btn !== undefined) {
            modal.find('.modal_submit').text(btn);
        }

        // Показываем модальное окно
        // с формой захвата
        $.fancybox.open({
            src: "#modal_form",
            type: "inline"
        })
    });


    /**
     *  Инициализация одностраничной навигации
     *  при помощи плагина One-page-nav
     */
    $('#topNav').singlePageNav({
        currentClass: 'current',
        updateHash: true,
        offset: 120,
        speed: 450,
        threshold: 0.5,
        filter: '',
        easing: 'swing'
    });


    /**
     * Валидация формы и отправка данных
     * на сервер при успешной валидации
     */

    $(".orderForm").validate({
        submitHandler: function(form) {
            var link = $(form).attr('action');
            var data = $(form).serialize();

            var name = $(form).find('input[name=name]').val();
            var email = $(form).find('input[name=email]').val();
            var phone = $(form).find('input[name=phone]').val();


            try {
                //ga('send', 'event', 'target_name', 'Send_Form');
                //yaCounter27354854.reachGoal('target_name');
            } catch (e) {
                console.log(e);
            }


            $.ajax({
                url: link,
                data: data,
                method: "POST",
                success: function(response){
                }
            });

            return false;
        }
    });



});


/**
 * jQuery validation default settings
 * https://jqueryvalidation.org/jQuery.validator.setDefaults/
 */

jQuery.validator.setDefaults({
    errorElement: 'span',
    errorPlacement: function(label, element) {
        label.addClass('form-error');
        label.insertAfter(element);
    },
    wrapper: 'span',
    rules: {
        name: {
            required: true
        },
        phone: {
            required: true,
            minlength: 10
        },
        email: {
            required: true,
            email:true
        },
        personalAgreement: {
            required: true
        }
    },
    messages: {
        name: {
            required: "Это поле обязательно для заполнения"
        },
        phone: {
            required: 'Это поле обязательно для заполнения',
            min: 'Телефон минимум 10 символов'
        },
        email: {
            required: "Это поле обязательно для заполнения",
            email: "Введите правильный email"
        },
        personalAgreement: {
            required: "Согласие на обработку персональных данных обязательно"
        }
    }
});


// Yandex MAP

$(function () {

    ymaps.ready(function () {
        var myMap = new ymaps.Map('map', {
                center: [56.172603, 40.448600],
                zoom: 16
            }, {
                searchControlProvider: 'yandex#search'
            }),

            // Создаём макет содержимого.
            MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
                '<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
            ),

            myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
                hintContent: 'Собственный значок метки',
                balloonContent: 'Это красивая метка'
            }, {
                // Опции.
                // Необходимо указать данный тип макета.
                iconLayout: 'default#image',
                // Своё изображение иконки метки.
                iconImageHref: 'img/marker.svg',
                // Размеры метки.
                iconImageSize: [60, 70],
                // Смещение левого верхнего угла иконки относительно
                // её "ножки" (точки привязки).
                iconImageOffset: [-30, -68]
            }),

            myPlacemarkWithContent = new ymaps.Placemark([55.661574, 37.573856], {
                hintContent: 'Собственный значок метки с контентом',
                balloonContent: 'А эта — новогодняя',
                iconContent: '12'
            }, {
                // Опции.
                // Необходимо указать данный тип макета.
                iconLayout: 'default#imageWithContent',
                // Своё изображение иконки метки.
                iconImageHref: 'images/ball.png',
                // Размеры метки.
                iconImageSize: [48, 48],
                // Смещение левого верхнего угла иконки относительно
                // её "ножки" (точки привязки).
                iconImageOffset: [-24, -24],
                // Смещение слоя с содержимым относительно слоя с картинкой.
                iconContentOffset: [15, 15],
                // Макет содержимого.
                iconContentLayout: MyIconContentLayout
            });

        myMap.geoObjects
            .add(myPlacemark)
            .add(myPlacemarkWithContent);
        myMap.behaviors.disable('scrollZoom');
    });

});