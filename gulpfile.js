'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    prefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    sourcemaps = require('gulp-sourcemaps'),
    rigger = require('gulp-rigger'),
    cssmin = require('gulp-minify-css'),
    cleanCSS = require('gulp-clean-css'),
    imagemin = require('gulp-imagemin'),
    svgo = require('gulp-svgo'),
    rimraf = require('rimraf'),
    browserSync = require("browser-sync"),
    less = require('gulp-less'),
    concat = require('gulp-concat'),
    spritesmith = require('gulp.spritesmith'),
    merge = require('merge-stream'),
    mainBowerFiles = require('main-bower-files'),
    plumber = require('gulp-plumber'),
    newer = require('gulp-newer'),
    reload = browserSync.reload;



var path = {
    build: {
        html: 'build/',
        js: 'build/js/',
        css: 'build/css/',
        img: 'build/img/',
        php: 'build/php/',
        fonts: 'build/fonts/'
    },
    src: {
        html: 'src/*.html',
        js: ['src/js/**/*.js','!src/js/main.js'],
        style: 'src/style/main.less',
        img: 'src/img/**/*.*',
        php: 'src/php/**/*.*',
        fonts: 'src/fonts/**/*.*'
    },
    watch: {
        html: 'src/**/*.html',
        js: 'src/js/main.js',
        style: 'src/style/**/*.less',
        img: 'src/img/**/*.*',
        php: 'src/php/**/*.*',
        fonts: 'src/fonts/**/*.*'
    },
    clean: './build'
};



var config = {
    server: {
        baseDir: "./build"
    },
    tunnel: false,
    host: 'localhost',
    port: 9000,
    logPrefix: "frontendKit"
};


// ---- SYSTEM TASKS ---------
gulp.task('webserver', function () {
    browserSync(config);
});
gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});


gulp.task('html:build', function () {
    gulp.src(path.src.html)
        .pipe(rigger())
        .pipe(gulp.dest(path.build.html))
        .pipe(reload({stream: true}));
});




// ------- BUILD JAVASCRIPT -------------
gulp.task('js:buildVendor', function () {
    gulp.src(path.src.js)
        .pipe(plumber())
        //.pipe(sourcemaps.init())
        //.pipe(concat('bundle.js'))
        .pipe(uglify())
        //.pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.js))
        .pipe(reload({stream: true}));
});
gulp.task('js:buildMain', function () {
    gulp.src('src/js/main.js')
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.js))
        .pipe(reload({stream: true}));
});
gulp.task('js:build', ['js:buildMain', 'js:buildVendor']);



// ---------- BUILD STYLES --------------
gulp.task('style:build', function () {
    gulp.src(path.src.style)
        .pipe(plumber())
        .pipe(less({
            includePaths: ['src/style/'],
            outputStyle: 'compressed',
            sourceMap: true,
            errLogToConsole: true
        }))
        .pipe(prefixer())
        .pipe(cssmin())
        .pipe(cleanCSS({compatibility: 'ie9'}))
        .pipe(gulp.dest(path.build.css))
        .pipe(reload({stream: true}));
});


// ----------- IMAGES BUILD ----------------
gulp.task('image:build', function () {
    gulp.src(path.src.img)
        .pipe(newer(path.build.img))
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            interlaced: true
        }))
        .pipe(svgo())
        .pipe(gulp.dest(path.build.img))
        .pipe(reload({stream: true}));
});



gulp.task('fonts:build', function() {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
});
gulp.task('php:build', function() {
    gulp.src(path.src.php)
        .pipe(gulp.dest(path.build.php))
});




/*
 * Задачи для автоматического переноса main
 * файлов из bower_components в папку src/../vendor/
 */
gulp.task('bower:mainJs', function() {
    return gulp.src(mainBowerFiles('**/*.js'))
        .pipe(gulp.dest('src/js/vendor/'))
});
gulp.task('bower:mainStyle', function() {
    return gulp.src(mainBowerFiles(['**/*.css','**/*.less']))
        .pipe(gulp.dest('src/style/vendor/'))
});
gulp.task('bower:main', ['bower:mainJs', 'bower:mainStyle']);




gulp.task('build', [
    'bower:main',
    'image:build',
    'html:build',
    'js:buildVendor',
    'js:buildMain',
    'style:build',
    'php:build',
    'fonts:build',
]);



gulp.task('watch', function(){
    watch([path.watch.html], function(event, cb) { gulp.start('html:build'); });
    watch([path.watch.style], function(event, cb) { gulp.start('style:build'); });
    watch([path.watch.js], function(event, cb) { gulp.start('js:buildMain'); });
    watch([path.watch.img], function(event, cb) { gulp.start('image:build'); });
    watch([path.watch.fonts], function(event, cb) { gulp.start('fonts:build'); });
    watch([path.watch.php], function(event, cb) { gulp.start('php:build'); });
});


gulp.task('default', ['build', 'webserver', 'watch']);


// -------- TEMP TASKS ------------------
// Sprite generator task
 gulp.task('sprite', function () {
     var spriteData = gulp.src('src/tmp/icons/*.png').pipe(spritesmith({
         imgName: 'sprite.png',
         cssName: 'sprite.less',
         padding: 3,
         imgPath: '../img/sprite.png'
     }));
     var imgStream = spriteData.img
        .pipe(gulp.dest('src/img/'));
     var cssStream = spriteData.css
        .pipe(gulp.dest('src/style/'));
     return merge(imgStream, cssStream);
 });
